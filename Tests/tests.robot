*** Settings ***
Documentation    Suite description
Library  SeleniumLibrary
Test Setup  Begin Web Test
Test Teardown  End Web Test

*** Test Cases ***

Kundvagn
    [Tags]  test1
    Input Text  id:twotabsearchtextbox  ferrari 458
    Click Button  xpath://*[@id="nav-search"]/form/div[2]/div/input
    Scroll Element Into View  link:Ferrari 458 Italia Red 1/24 by Bburago 26003
    Click Link  link:Ferrari 458 Italia Red 1/24 by Bburago 26003
    Click Button  id:add-to-cart-button
    ${cart_amount} =    Get Text  id:nav-cart-count
    Should Be Equal  ${cart_amount}  1
    Click Element    id:hlb-view-cart-announce
    ${cart_item} =      Get Text  xpath://span[contains(text(),'Ferrari 458')]
    Should Be Equal     ${cart_item}    Ferrari 458 Italia Red 1/24 by Bburago 26003
    #Page Should Contain  Ferrari 458 Italia Red 1/24 by Bburago 26003

Kryssruta
    [Tags]  test2
    Click Element   xpath://a[contains(@aria-label, 'Computers')]
    # Alternativ xpath, hittas genom att klicka på Copy full XPath
    # //div[1]/div[1]/div[3]/div[1]/div[1]/div/div[2]/div[1]/div[1]/a
    Checkbox Should Not Be Selected     name:s-ref-checkbox-16741513011
    Click Element  name:s-ref-checkbox-16741513011
    Scroll Element Into View  id:p_n_amazon_certified/16741513011
    Checkbox Should Be Selected     xpath://*[@id="p_n_amazon_certified/16741513011"]/span/a/div/label/input

Gherkin
    [Tags]  test3
    Given user enters a search query
    When user clicks search button
    Then the text "results for "${SEARCH_TERM}"" appears

Popup
    [Tags]  test4
    Set Selenium Timeout    10 seconds
    Wait Until Element Is Not Visible  xpath://*[@id="nav-signin-tooltip"]/a/span
    Click Element   xpath://*[@id="navSwmHoliday"]
    # Alternativ 1
    Wait Until Page Contains    Deals and Promotions
    # Alternativ 2
    # ${page_title} =  Get Text   xpath://*[@id="fst-hybrid-dynamic-h1"]/div/h1/b
    # Should Be Equal  ${page_title}  Deals and Promotions

Negativt testfall
    [Tags]  test5
    Input Text  id:twotabsearchtextbox  ferrari 458
    Click Button  xpath://*[@id="nav-search"]/form/div[2]/div/input
    Scroll Element Into View  link:Ferrari 458 Italia Red 1/24 by Bburago 26003
    Click Link  link:Ferrari 458 Italia Red 1/24 by Bburago 26003
    Click Button  id:add-to-cart-button
    Click Element    id:hlb-view-cart-announce
    Click Element   xpath://span[@class='a-button-text a-declarative']
    ${new_amount} =     Set Variable    9
    Click Link  ${new_amount}
    ${cart_amount} =    Get Text  id:nav-cart-count
    Should Not Be True  '${cart_amount}'=='${new_amount}'



*** Keywords ***
Begin Web Test
    Open Browser    about:blank  chrome
    Go To   http://www.amazon.com
    Maximize Browser Window

End Web Test
    Close Browser

user enters a search query
    Input Text  id:twotabsearchtextbox  ${SEARCH_TERM}

user clicks search button
    Click Button  xpath://*[@id="nav-search"]/form/div[2]/div/input

the text "results for "${SEARCH_TERM}"" appears
    Wait Until Page Contains  results for "${SEARCH_TERM}"


*** Variables ***
${SEARCH_TERM} =  ferrari 458